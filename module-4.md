## Module 4 Notes

### Multivariable Calculus

Suppose you have two products (x, y),
Example cost function with two products in the formula (product X and product Y)

| Name | Reference |
|---|---|
| Cost for product X and Y | $`C(x,y)`$ |
| Price for product X | $`p`$ |
| Price for product Y | $`q`$ |
| Revenue for product X and Y | $`R(x,y)=xp+yq`$ |
| Profit for product X and Y | $`P(x,y)=R(x,y)-C(x,y)`$ |

### Cobb-Douglas Equation

$`f(x,y)=ax^{b}y^c`$ where `a`, `b`, `c` are constants

### Partial Derivatives

When solving partial derivatives, treat all other variables as constants.

Example 1:

$`f(x,y)=700 + 70x + 100y`$

$`\frac{df}{dx}=f_x(x,y)=70`$

$`\frac{df}{dy}=f_y(x,y)=100`$

Example 2:

$`f(x,y)=140x+200y-4x^2+2xy-12y^2-700`$

$`\frac{df}{dx}=f_x(x,y)=140-8x+2y`$

$`\frac{df}{dy}=f_y(x,y)=200+2x-24y`$

### Gradient
$`\nabla{f}=\begin{pmatrix}f_x\\f_y\end{pmatrix}`$

If

$`f_x(4000,2500)=4.53`$
and
$`f_y(4000,2500)=10.86`$

Then

$`\nabla{f}(4000,2500)=\begin{pmatrix}4.53\\10.86\end{pmatrix}`$

### Vector Notation

Example:

$`(2 - 1)\begin{pmatrix}4.53\\10.86\end{pmatrix}=2(4.53)-1(10.86)`$

### Critical Points

Critical point exists when $`f_x=0`$ and $`f_y=0`$

### Second Partial Derivatives

For two variables, there are four second partials:

| Partial of | with respect to | denoted as |
|---|---|---|
| $`f_x`$ | `x` | $`f_{xx}`$ or $`\frac{d^2f}{dx^2}`$ |
| $`f_x`$ | `y` | $`f_{xy}`$ or $`\frac{d^2f}{dxdy}`$ |
| $`f_y`$ | `x` | $`f_{yx}`$ or $`\frac{d^2f}{dydx}`$ |
| $`f_y`$ | `y` | $`f_{yy}`$ or $`\frac{d^2f}{dy^2}`$ |

For most functions $`f_{xy}=f_{yx}`$

### Matrix form: Hessian

$`\begin{pmatrix}f_{xx} f_{xy}\\f_{yx} f_{yy}\end{pmatrix}`$

### Second Derivative Test (Determinant of the Hessian)

$`D(x,y)=f_{xx}(x,y)f_{yy}(x,y)-f_{xy}(x,y)f_{yx}(x,y)`$

| If | Then |
|---|---|
| $`D(x,y) < 0`$ | saddle point |
| $`D(x,y) = 0`$ | ??? |
| $`D(x,y) > 0`$ | Look at $`f_{xx}`$ |

| Looking at $`f_{xx}`$, If | Then |
|---|---|
| $`f_{xx}(x,y) < 0`$  | critical point is a maximum |
| $`f_{xx}(x,y) > 0`$  | critical point is a minimum |

### Global max/min

For all $`(x,y)`$ in the domain

| If | Convexity | Notes |
|---|---|---|
| $`D(x,y) > 0`$ and $`f_{xx} < 0`$ | concave downwards (concave) | local max = global max |
| $`D(x,y) > 0`$ and $`f_{xx} > 0`$ | concave upwards (convex) | local min = global min |

### Indefinite Integrals

If $`f(x)=F'(x)`$ then $`\int{f(x)dx}=F(x)+C`$ where `C` is a constant

| Integral | Definition |
|---|---|
| $`\int{\ln{x}}dx`$ | $`xlnx + C`$ |
| $`\int{x^n}dx`$ | $`\frac{x^{n+1}}{n+1}+C`$ |
| $`\int{\frac{1}{x}}dx`$ | $`\ln{\lvert x \rvert}+C`$ |
| $`\int{e^x}dx`$ | $`e^x+C`$ |
| $`\int{1}dx`$ | $`x+C`$ |

| Integral | Definition |
|---|---|
| $`\int{[f(x)+g(x)]}dx`$ | $`\int{f(x)}dx+\int{g(x)}dx`$  |
| $`\int{kf(x)}dx`$ | $`k\int{f(x)}dx`$  |

### Definite Integrals

There are no `C`'s in calculating definite integrals because they cancel out.

### Two dimensional approximation

$`f(x+A,y+B) \approx f(x,y)+f_x(x,y)*A+f_y(x,y)*B`$
