## Module 2 Notes

### Basic Rules 

| Name | $`f(x)`$ | $`f'(x)`$ |
|---|---|---|
| Constant | $`f(x) = c`$  | $`f'(x) = 0`$ |
| Linear | $`f(x) = ax`$  | $`f'(x) = a`$ |
| Power | $`f(x) = x^b`$  | $`f'(x) = bx^{b-1}`$ |
| Euler | $`f(x) = e^x`$  | $`f'(x) = e^x`$ |
| Other bases | $`f(x) = b^x`$  | $`f'(x) = b^x\ln{b}`$ |
| Logarithms(e) | $`f(x) = \ln{x}`$  | $`f'(x) = \frac{1}{x}`$ |
| Logarithms(b) | $`f(x) = \log_{b}(x)`$  | $`f'(x) = \frac{1}{x\ln{b}}`$ |

### Combination Rules

| Name | $`f(x)`$ | $`f'(x)`$ |
|---|---|---|
| Sum | $`f(x) = g(x)+h(x)`$  | $`f'(x) = g'(x)+h'(x)`$ |
| Difference | $`f(x) = g(x)-h(x)`$  | $`f'(x) = g'(x)-h'(x)`$ |
| Product | $`f(x) = g(x)h(x)`$  | $`f'(x) = g(x)h'(x)+g'(x)h(x)`$ |
| Quotient | $`f(x) = \frac{g(x)}{h(x)}`$  | $`f'(x) = \frac{h(x)g'(x)-g(x)h'(x)}{(h(x))^2}`$ |
| Composition | $`f(x) = g(h(x))`$  | $`f'(x) = g'(h(x))h'(x)`$ |

### Approximation
$`f(x + d) \approx f(x) + df'(x)`$
