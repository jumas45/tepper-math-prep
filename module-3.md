## Module 3 Notes

### Critical Points

Critical point(s) exist for all `x` where $`f'(x)=0`$

### First Derivative Test

1) Calculate $`f'(x)`$
1) Find critical points
1) Use surrounding points to calculate min/max/inflection

| If (where `y` is whatever you define, usually +/- 1) | Then |
|---|---|
| $`f(x-y) < f(x) > f(x+y)`$ |  relative max  |
| $`f(x-y) > f(x) < f(x+y)`$ |  relative min |
| otherwise |  inflection |

### Second Derivative Test

| If (for critical point(s) `x`) | Then |
|---|---|
| $`f"(x)=0`$ |  inflection point |
| $`f"(x)<0`$ |  relative max |
| $`f"(x)>0`$ |  relative min |

### Global max and min

You will be given a domain [`minX`, `maxX`]

1) Calculate $`f'(x)`$
1) Find critical points
1) Substitute critical point x values, `minX` and `maxX` to determine the corresponding Y values
1) Highest Y value will be the global max. Lowest Y value will be the global min.

### Optimization of Composition (Shortcut)
Let `f` be a strictly increasing function, then `x` is local min for $`g(x)`$ if and only if `x` is the local min for $`f(g(x))`$.

For example:
$`g(x)`$ and $`e^{g(x)}`$ have the same local min (or max)

### Convexity and Concavity (Concave up and concave down)

| If (for all `x`) | Then |
|---|---|
| $`f"(x) >= 0`$ for all `x`  | Convex (concave up) |
| $`f"(x) <= 0`$ for all `x`  | Concave (concave down) |
